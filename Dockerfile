FROM alpine:latest

RUN mkdir /build && \
	wget -q https://github.com/flightaware/dump1090/archive/v3.8.0.tar.gz -O - \
	| tar -xzC /build && \
	wget http://repo.feed.flightradar24.com/pool/raspberrypi-stable/f/fr24feed/fr24feed_1.0.23-8_armhf.deb \
	-O /build/fr24feed.deb && \
	apk add --no-cache --repository \
	'http://dl-cdn.alpinelinux.org/alpine/edge/testing' \
	librtlsdr-dev && \
    apk add --no-cache \
    make \
    gcc \
    musl-dev \
    ncurses-dev \
    ncurses-libs \
    binutils \
    bash && \
    cd /build/dump1090-3.8.0 && \
	make BLADERF=no dump1090 view1090 faup1090 && \
	cp dump1090 /usr/bin/dump1090 && \
    cp faup1090 /usr/bin/faup1090 && \
    cp view1090 /usr/bin/view1090 && \
    cd /build && \
	ar x fr24feed.deb && \
    tar -xzf data.tar.gz && \
    cp usr/bin/fr24feed /usr/bin && \
    cd / && \
    rm -rf /build && \
    apk del --no-cache --rdepends \
    gcc \
    make \
    libc-dev \
    ncurses-dev \
    binutils

ADD fr24feed.ini /etc/fr24feed.ini
COPY run.sh /
RUN chmod +x /run.sh
EXPOSE 8080 8754
CMD [ "/run.sh" ]