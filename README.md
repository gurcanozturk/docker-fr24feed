FlightRadar24 ADB-S receiver Docker image based on Alpine Linux, dump1090 and fr24feeder runs on Raspberry Pi.




Build it.
docker build -t fr24feed .


Run it.
docker run -d -p 8754:8754 -p 8080:8080 --device=/dev/bus/usb/001/005 fr24feed


Watch it.
http://docker-ip:8754